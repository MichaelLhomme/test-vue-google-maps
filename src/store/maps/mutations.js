import Vue from 'vue'

export function setPosition (state, position) {
  state.position = position
}

export function addMarker (state, marker) {
  state.markers.push(marker)
}

export function updateMarker (state, marker) {
  const idx = state.markers.findIndex(m => m.position.lat === marker.position.lat && m.position.lng === marker.position.lng)
  state.markers[idx] = marker
}

export function deleteMarker (state, marker) {
  const idx = state.markers.findIndex(m => m.position.lat === marker.position.lat && m.position.lng === marker.position.lng)
  state.markers.splice(idx, 1)
}
