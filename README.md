# Test Vue Google Maps (test-vue-google-maps)

A test integration of Google Maps JS into a Vue project.

Google Maps API usage requires a personal Google API KEY, this app manages it using a `.env` file :
- copy the `env.template` file to `.env`
- edit the file and set your Google API KEY

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v1.quasar.dev/quasar-cli/quasar-conf-js).
